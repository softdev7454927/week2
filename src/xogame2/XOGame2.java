/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package xogame2;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class XOGame2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private static char currentPlayer = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();

        while (true) {
            printTable();
            printTurn();
            inputRowCol();
            if (isWin()) {
                printTable();
                printWin();
                if(playAgmain()){
                    resetTable();
                }else {
                    break;
                }
               
            }
            if (isDraw()) {
                printTable();
                printDraw();
                if(playAgmain()){
                    resetTable();
                }else {
                    break;
                }
            }
            switchPlayer();

        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private static void printTable() {
        System.out.println("*******");
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + "|");
            }
            System.out.println("");
        }
        System.out.println("*******");
    }

    private static void printTurn() {
        System.out.println(currentPlayer + " turn");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row and column :");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }else {
                System.out.println("Try again");
            }

        }
    }

    private static void switchPlayer() {
        currentPlayer = currentPlayer == 'X' ? 'O' : 'X';
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkDiagonals()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println("Player " + currentPlayer + " Win!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonals() {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        } else if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("The game is a draw!!!");
    }

    private static boolean playAgmain(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to play again? (y/n): ");
        String newGame = kb.next();
        if (newGame.equalsIgnoreCase("y")) {
            return true;
        } else {
            System.out.println("Thank you for playing! Goodbye!");
            return false;
        }

    }

    private static void resetTable() {
        table = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentPlayer = 'O';
    }

}
